
# --- !Ups

ALTER TABLE task ADD started timestamp;
ALTER TABLE task ADD finished timestamp;

# --- !Downs

ALTER TABLE task DROP started;
ALTER TABLE task DROP finished;
