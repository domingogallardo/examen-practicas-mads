import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json.{Json, JsValue}

import org.specs2.mutable._
import org.specs2.runner._
import org.specs2.matcher._

import models.Task

class ControllersTests extends Specification with JsonMatchers {

  "Controllers" should {

    "devolver una tarea en formato JSON con un GET /tasks/<id>" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
 
            val taskId = Task.create("prueba","anonymous")
            val Some(resultTask) = route(FakeRequest(GET, "/tasks/"+taskId))

            status(resultTask) must equalTo(OK)
            contentType(resultTask) must beSome.which(_ == "application/json")

            val resultJson: JsValue = contentAsJson(resultTask)
            val resultString = Json.stringify(resultJson) 

            resultString must /("id" -> taskId)
            resultString must /("label" -> "prueba")
            resultString must /("taskOwner" -> "anonymous")

         }
      }
  }
}

