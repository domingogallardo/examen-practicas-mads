# Play TodoList

Una sencilla aplicación play dearrollada en las prácticas de la asignatura [Metodologías Ágiles de Desarrollo del Software](http://www.dccia.ua.es/dccia/inf/asignaturas/MADS/) de la Universidad de Alicante.

## API REST

La aplicación define un API REST para trabajar con una lista de tareas pendientes de hacer.
Una línea más
### Usuarios

Cada tarea guarda el usuario que la ha creado. Existe un usuario "anonimo" para los casos en que no se proporciona el usuario propietario de la tarea.

## Heroku

Puedes probar el despliegue en Heroku pulsando en [este enlace](http://immense-reaches-4872.herokuapp.com/).

